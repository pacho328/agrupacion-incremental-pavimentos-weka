/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pavimentosweka;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Scanner;
import weka.clusterers.SimpleKMeans;
import weka.core.Instances;
import weka.classifiers.functions.LinearRegression;
import weka.core.converters.ConverterUtils.DataSource;
import weka.filters.unsupervised.attribute.Normalize;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.classifiers.Evaluation;
import weka.core.Instance;
import weka.core.Attribute;
import weka.core.converters.CSVSaver;
/**
 *
 * @author Pacho328
 */
public class WekaPaviments {
    public ArrayList<Instances> datalist = new ArrayList<Instances>();
    public ArrayList<LinearRegression> lrmlist = new ArrayList<LinearRegression>();
    public ArrayList<Instances> clusterslist = new ArrayList<Instances>();
    public ArrayList<Instances> clustersSelected = new ArrayList<Instances>();
    public ArrayList<Double> ccSeleted = new ArrayList<Double>();
    public ArrayList<Instances> clustersFree = new ArrayList<Instances>();
    public ArrayList<Double> ccFree = new ArrayList<Double>();
    public ArrayList<LinearRegression>  lrList = new ArrayList<LinearRegression>();


    public ArrayList<LinearRegression> lrmclusters = new ArrayList<LinearRegression>();
    private ArrayList<Instances> listTemp = new ArrayList<Instances>();

    public int numberOfClusters;
    public int numData;
    public double fitness;
    public Instances struct;  
    private Instances dataNormalizate;
    public double[] correlations;
    public double correlation;
    public double indice2CC = 0.75;
    public double indiceCC;




    public ArrayList<Instances> getListTemp() {
        return listTemp;
    }
    
    public WekaPaviments(int numberOfClusters, int numData,double icc) throws Exception {
        this.numberOfClusters = numberOfClusters;
        this.numData = numData;
        this.correlations = new double[numberOfClusters];
        DataSource ds2 = new DataSource("src/pavimentosweka/data_No_timePeriod.arff");
        DataSource dsN = new DataSource("src/pavimentosweka/dataNormalizate.arff");
        this.indiceCC = icc;
        Instances datat = ds2.getDataSet();
        Instances dataN = dsN.getDataSet();
        datat.setClassIndex(1);
        datat = createIndex(datat); // Establecemos al psi como Label   
        dataN.setClassIndex(1); // Establecemos al psi como Label 
        this.struct = datat;
        this.dataNormalizate = dataN;
        this.correlation =0;
    }

    public Instances getDataNormalizate() {
        return dataNormalizate;
    }

    public Instances getStruct() {
        return struct;
    }
    
    
    
    public void evaluateModel (LinearRegression rlm, Instances data) throws Exception {
        
        Evaluation lreval = new Evaluation(data);
        lreval.evaluateModel(rlm, data);
        //System.out.println(lreval.toSummaryString());
    }
    public void evaluateModelClu (LinearRegression rlm, Instances data) throws Exception {
        
        Evaluation lreval = new Evaluation(data);
        lreval.evaluateModel(rlm, data);
       // System.out.println(lreval.toSummaryString());

    }
    
    public int[] saveIndex (Instances data) throws Exception {
        int[] ids = new int[data.numInstances()];
        //guarda los identificadores de los registros
        for (int i = 0; i < data.numInstances(); i++) {
            ids[i] = (int) data.get(i).value(0);
        }
        data.deleteAttributeAt(0); // elimina el id antes de la regresion lineal 
       return ids;
    }
    public int[] getIndex (Instances data) throws Exception {
        int[] ids = new int[data.numInstances()];
        //guarda los identificadores de los registros
        for (int i = 0; i < data.numInstances(); i++) {
            ids[i] = (int) data.get(i).value(0);
        }
        
       return ids;
    }
    
    public LinearRegression calculateRML(Instances data) throws Exception {
        Instances data2 = new Instances(data,0,data.numInstances());
        int[] ids = saveIndex(data2);
        LinearRegression lr = new LinearRegression(); // utilice por defecto metodos de seccion de atributis
        String[] option =  {"-S","0"};
//        System.out.println(data2.lastInstance());
        lr.setOptions(option);
        lr.buildClassifier(data2);
        evaluateModel(lr,data2);
        restoreIndex(data2,ids);//recupera los identificadores de los registros
        return lr;
    }
      public LinearRegression calculateRMLClu(Instances data,int i) throws Exception {
        int[] ids = saveIndex(data);
        if(data.numAttributes()>15){
        data.deleteAttributeAt(15);
        data.deleteAttributeAt(15);
        }
        LinearRegression lr = new LinearRegression(); // utilice por defecto metodos de seccion de atributis
        String[] option =  {"-S","0"};
        lr.setOptions(option);
        lr.buildClassifier(data);
        evaluateModelClu(lr,data);
        restoreIndex(data,ids);//recupera los identificadores de los registros
        return lr;
    }
    
    public Instances initializeDataSet() throws Exception{      
        DataSource ds = new DataSource("src/pavimentosweka/data_No_timePeriod.arff");
        Instances data = ds.getDataSet();
        data.deleteAttributeAt(0);
        data.setClassIndex(0); // Establecemos al psi como Label
        return data;
    }
    public Instances initializeDataTest() throws Exception{      
        DataSource ds = new DataSource("src/pavimentosweka/data_test.arff");
        Instances dataTest = ds.getDataSet();
        dataTest.setClassIndex(1); // Establecemos al psi como Label
        return dataTest;
    }
    public Instances normalizeDataSet(Instances data) throws Exception{      
        Normalize normalize = new Normalize();
        normalize.setInputFormat(data);
        Instances newdata = Filter.useFilter(data, normalize);
        return newdata;
    }
    public Instances predicting(Instances dataset, LinearRegression rlm, int it) throws Exception{
        int[] ids = saveIndex(dataset);
        Attribute psi_es = new Attribute("PSI_est");
        Attribute err_abs = new Attribute("err_abs");
        dataset.insertAttributeAt(psi_es, 15);
        dataset.insertAttributeAt(err_abs, 16);          
        int i ;
        double psi_e;
        for(i=0;i< dataset.numInstances();i++){
            Instance row = dataset.get(i);
            psi_e   = rlm.classifyInstance(row);
            row.setValue(15, psi_e);       
            row.setValue(16, Math.abs((row.value(0)-psi_e)));
        }
        restoreIndex(dataset,ids);
        return dataset;
    }
    public ArrayList<Instances> createClusters(int k, int tam, Instances data) throws Exception{
        ArrayList<Instances> datalist = new ArrayList<Instances>();
        int cant = tam/k,i,cont=0,it=0;
        Instances train;
        for(i=0;i<=tam-cant;i=i+cant){
            if(it!=k-1){
                train = new Instances(data, i, cant); 
                datalist.add(train);
                it++;      
            }else{
                train = new Instances(data,i, tam-i); 
                datalist.add(train); 
            }
        }
        return datalist;
    } 
    public LinearRegression[] calculateRMLCluseters(ArrayList<Instances> datalist) throws Exception{
        int i;
        LinearRegression[] rlms = new LinearRegression[datalist.size()]; 
        for(i=0;i<datalist.size();i++){
               rlms[i] =calculateRML(datalist.get(i));
        }
        
        return rlms;
    }
    public void calculateRMLPreditions(ArrayList<Instances> datalist, LinearRegression[] rlms) throws Exception{
        int i;
        for(i=0;i<datalist.size();i++){
               datalist.set(i, predicting(datalist.get(i),rlms[i],i));
        }
        
    }
    public void show(ArrayList<Instances> datalist){
        for (int i = 0; i < datalist.size(); i++) {
            System.out.println(datalist.get(i).lastInstance());
        }             
    }
    public Instances sortByError (Instances data) throws Exception {
        data.sort(17);
        if(data.numAttributes()!=16){
            data.deleteAttributeAt(16);
            data.deleteAttributeAt(16);
        }
        return data;
    }
     public Instances sortDataset (Instances data, LinearRegression rlm) throws Exception {
        data = predicting(data,rlm,0);
        data.sort(16);
        return data;
    }
    
     public Instances createIndex (Instances data) throws Exception {
        Attribute id = new Attribute("id");
        data.insertAttributeAt(id, 0);
         for (int i = 0; i < data.numInstances(); i++) {
             data.get(i).setValue(0, i);
         }
        return data;
    }
     public static Instances restoreIndex (Instances data, int[] ids) throws Exception {
        Attribute id = new Attribute("id");
        data.insertAttributeAt(id, 0);
         for (int i = 0; i < data.numInstances(); i++) {
             data.get(i).setValue(0, ids[i]);
         }
        return data;
    }
    public Instances deleteIndex (Instances data) throws Exception {
        data.deleteAttributeAt(0);
        return data;
    }
    public Instances saveRemoveCluster (Instances data, LinearRegression lrm, int k ) throws Exception {    
        Instances copy = new Instances(data,0,data.numInstances());
        this.datalist.add(copy);
        this.lrmlist.add(lrm);
        int cant = this.numData/this.numberOfClusters,i,cont=0,it=0;
        cant = (k==(this.numberOfClusters-1))? data.numInstances(): cant;
        Instances cluster = new Instances(data,0,cant);
        LinearRegression rlmc =  calculateRMLClu(cluster,k);
        this.lrmclusters.add(rlmc);
        cluster = predicting(cluster,rlmc,1);
        this.clusterslist.add(cluster);
        int[] ids = getIndex(cluster);
        if(k!=(this.numberOfClusters-1)){
            
            for (int j = 0; j < cant; j++) {
                for (int l = 0; l < ids.length; l++) {
                   if(data.get(j).value(0)==ids[l]){
                       data.lastInstance();
                       data.delete(j);
                   } 
                }
            }
        }   
        

        return data;
    }

    public ArrayList<Instances> getClusterslist() {
        return clusterslist;
    }

    public void setClusterslist(ArrayList<Instances> clusterslist) {
        this.clusterslist = clusterslist;
    }

    public ArrayList<LinearRegression> getLrmclusters() {
        return lrmclusters;
    }

    public void setLrmclusters(ArrayList<LinearRegression> lrmclusters) {
        this.lrmclusters = lrmclusters;
    }
    
    public void setDatalist(ArrayList<Instances> datalist) {
        this.datalist = datalist;
    }

    public void setLrmlist(ArrayList<LinearRegression> lrmlist) {
        this.lrmlist = lrmlist;
    }

    public ArrayList<Instances> getDatalist() {
        return datalist;
    }

    public ArrayList<LinearRegression> getLrmlist() {
        return lrmlist;
    }
    
    
    
    
    public int[] getPositionClusters() throws IOException{
        
        int[] position = new int[this.numData];
        
        for (int i = 0; i < this.clusterslist.size(); i++) {
            for (int j = 0; j < clusterslist.get(i).numInstances(); j++) {
                position[(int)this.clusterslist.get(i).get(j).value(0)] = i;
            }
        }
        return position;
    }
    
    public ArrayList<Instances> updateCluter(int[] pos ){
        
        for (int i = 0; i < this.numberOfClusters; i++) {
            Instances copy = this.struct.stringFreeStructure();             
            for (int j = 0; j < pos.length; j++) {
                if(pos[j]==i){
                    copy.add(searchID(j));
                }
            }
            this.listTemp.add(copy);
        }

    return this.listTemp;
    }
    public ArrayList<Instances> updateCluter2(int[] pos, Instance ins ){
        
        for (int i = 0; i < this.numberOfClusters-1; i++) {
            Instances copy = this.struct.stringFreeStructure();             
            for (int j = 0; j < pos.length; j++) {
                if(pos[j]==i){
                    copy.add(searchID(j));
                }
            }
            copy.add(ins);
            this.listTemp.add(copy);         
        }

    return this.listTemp;
    }
    
     public ArrayList<Instances> updateCluterNor(int[] pos,int numClusTem ){
        ArrayList<Instances> clustersNor = new ArrayList<Instances>();
        for (int i = 0; i < numClusTem; i++) {
            Instances copy = this.dataNormalizate.stringFreeStructure(); 
            for (int j = 0; j < pos.length; j++) {
                if(pos[j]==i){
                    copy.add(searchIDNor(j));
                }
            }
            clustersNor.add(copy);
        }

    return clustersNor;
    }
     public Instances calculateCentroidNor(ArrayList<Instances> clustersNor ){
        Instances copy = this.dataNormalizate.stringFreeStructure();
        for (int i = 0; i < clustersNor.size()-1; i++) {
            Instance Centroid = this.dataNormalizate.firstInstance();
            for (int k = 2; k < clustersNor.get(i).numAttributes(); k++) {
                   Centroid.setValue(k,clustersNor.get(i).meanOrMode(k) );
            }
            Centroid.setValue(1,0);
            copy.add(Centroid);
        }

    return copy;
    }
     public Instances calculateCentroid(ArrayList<Instances> clustersNor ){
        Instances copy = this.struct.stringFreeStructure();
        copy.deleteAttributeAt(0);
        copy.deleteAttributeAt(0);
        for (int i = 0; i < clustersNor.size(); i++) {
            clustersNor.get(i).deleteAttributeAt(0);
            Instances copyt = clustersNor.get(i);
            Instance Centroid = clustersNor.get(i).firstInstance();
            for (int k = 0; k < clustersNor.get(i).numAttributes(); k++) {
                   Centroid.setValue(k,clustersNor.get(i).meanOrMode(k) );
            }
            copy.add(Centroid);
        }
        
    return copy;
    }
    
    public Instance searchID(int val){
        Instances orginal =this.struct;
        
        for(Instance orgi: orginal){
            if(val==(int)orgi.value(0)){
             return orgi;
            }
        }
        return null;
    }
    
    public Instance searchIDNor(int val){
        Instances orginal = new Instances(this.dataNormalizate,0,this.dataNormalizate.numInstances());
        
        for(Instance orgi: orginal){
            if(val==(int)orgi.value(0)){
             return orgi;
            }
        }
        return null;
    }
    
    public double corelacionProm(ArrayList<Instances> datalist) throws Exception{
        double res ;
        double sum=0;
        double cc[] = new double[datalist.size()];
        int tr[] = new int[datalist.size()];
        this.ccFree.clear();
        for (int i = 0; i < datalist.size(); i++) {
            Instances copyTemp = new Instances(datalist.get(i),0,datalist.get(i).numInstances());
            int[] ids = saveIndex(copyTemp);
            if(copyTemp.numAttributes()>15){
                
                copyTemp.deleteAttributeAt(15);
                copyTemp.deleteAttributeAt(15);
            }
            LinearRegression lr = new LinearRegression(); // utilice por defecto metodos de seccion de atributis
            String[] option =  {"-S","0"};
            lr.setOptions(option);
            lr.buildClassifier(copyTemp);
            Evaluation lreval = new Evaluation(copyTemp);
            lreval.evaluateModel(lr, copyTemp);
            cc[i]=lreval.correlationCoefficient();
            tr[i]=copyTemp.numInstances();
            if(cc[i]>this.indiceCC){
                this.ccSeleted.add(cc[i]);
            }else{
                this.ccFree.add(cc[i]);
            }
            sum=sum+ (lreval.correlationCoefficient()* copyTemp.numInstances());
        }
        res = sum/ this.numData;
        this.correlations = cc;
        this.correlation = res; 
        saveResults(cc,tr,res);
        return res;
    }
     public ArrayList<LinearRegression> corelacionPromSummary(ArrayList<Instances> datalist) throws Exception{
        double res ;
        double sum=0;
        double cc[] = new double[datalist.size()];
        int tr[] = new int[datalist.size()];
        this.ccFree.clear();
        for (int i = 0; i < datalist.size(); i++) {
            Instances copyTemp = new Instances(datalist.get(i),0,datalist.get(i).numInstances());
            int[] ids = saveIndex(copyTemp);
            if(copyTemp.numAttributes()>15){
                copyTemp.deleteAttributeAt(15);
                copyTemp.deleteAttributeAt(15);
            }
            LinearRegression lr = new LinearRegression(); // utilice por defecto metodos de seccion de atributis
            String[] option =  {"-S","0"};
            lr.setOptions(option);
            lr.buildClassifier(copyTemp);
            this.lrList.add(lr);
            Evaluation lreval = new Evaluation(copyTemp);
            lreval.evaluateModel(lr, copyTemp);
            System.out.println("C"+i);
            System.out.println(lr.toString());
            System.out.println(lreval.toSummaryString());
            cc[i]=lreval.correlationCoefficient();
            tr[i]=copyTemp.numInstances();
            if(cc[i]>this.indiceCC){
                this.ccSeleted.add(cc[i]);
            }else{
                this.ccFree.add(cc[i]);
            }
            sum=sum+ (lreval.correlationCoefficient()* copyTemp.numInstances());
        }

        return this.lrList;
    }
    
    
    
     public double[] corelacionProm2(ArrayList<Instances> datalist) throws Exception{
        double res ;
        double sum=0;
        double cc[] = new double[datalist.size()];
        int tr[] = new int[datalist.size()];
        for (int i = 0; i < datalist.size(); i++) {
            Instances copyTemp = new Instances(datalist.get(i),0,datalist.get(i).numInstances());
            int[] ids = saveIndex(copyTemp);
            if(copyTemp.numAttributes()>15){
                copyTemp.deleteAttributeAt(15);
                copyTemp.deleteAttributeAt(15);
            }
           copyTemp.deleteAttributeAt(0);
            LinearRegression lr = new LinearRegression(); // utilice por defecto metodos de seccion de atributis
            String[] option =  {"-S","0"};
            lr.setOptions(option);
            lr.buildClassifier(copyTemp);
            Evaluation lreval = new Evaluation(copyTemp);
            lreval.evaluateModel(lr, copyTemp);
            cc[i]=lreval.correlationCoefficient();
            tr[i]=copyTemp.numInstances();
            sum=sum+ (lreval.correlationCoefficient()* copyTemp.numInstances());
        }
        this.correlations = cc;
        return cc;
    }
    
    public void saveResults(double[] vect,int[] tam,double pro )throws IOException{
        BufferedWriter br = new BufferedWriter(new FileWriter("results/C"+ Integer.toString(tam.length) +".txt"));
        StringBuilder sb = new StringBuilder();
        // Append strings from array
        for (int i = 0; i < vect.length; i++) {
            sb.append(Double.toString(vect[i]));
            sb.append(",");
            sb.append(Integer.toString(tam[i]));
            sb.append("\n");
        }
        sb.append(Double.toString(pro));
        br.write(sb.toString());
        br.close();
    
        
    }
    
    
    public double getFitness(int[] post) throws Exception {
        double res ;
     if(post!=null){ 

        ArrayList<Instances> listTemp = updateCluter(post);    
         res = corelacionProm(listTemp);
     }else{
         res = corelacionProm(this.clusterslist);
     }
     return res;
    }
    
    public double[] getFitness3(ArrayList<Instances> clist) throws Exception {
        double[] res ;
        res = corelacionProm2(clist);
        return res;
    }
    public double getFitness4(ArrayList<Instances> clist) throws Exception {
        double res ;
        res = corelacionProm(clist);
        return res;
    }
    
    public double[] getFitness2(int[] post, Instance ins) throws Exception {
        double []res ;
  

        ArrayList<Instances> listTemp = updateCluter2(post,ins);    
         res = corelacionProm2(listTemp);
     
     return res;
    }
    
     public ArrayList<LinearRegression> getFitnesSummary(ArrayList<Instances> clist) throws Exception {
        ArrayList<LinearRegression> res ;
         res = corelacionPromSummary(clist);
     
     return res;
    }
    
    public Instances updateClusters(ArrayList<Instances> datalist) throws Exception{
       Instances copy = datalist.get(0).stringFreeStructure();   
       copy.deleteAttributeAt(16);
       copy.deleteAttributeAt(16);
       this.clustersFree.clear();
       Remove remove = new Remove();
       int[] indices  = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
        for (int i = 0; i < datalist.size(); i++) {
             if(datalist.get(i).numAttributes()>16){
                remove.setAttributeIndicesArray(indices);
                remove.setInvertSelection(true);
                remove.setInputFormat(datalist.get(i));
                Instances newData = Filter.useFilter(datalist.get(i), remove);
                datalist.set(i, newData);
            }
             if(this.correlations[i]<this.indiceCC){
                 Instances free = new Instances(datalist.get(i),0,datalist.get(i).numInstances());
                 this.clustersFree.add(free);
                 for (int j = 0; j < datalist.get(i).numInstances(); j++) {
                    copy.add(datalist.get(i).get(j));
                 }
             }else{
                 Instances Seleted = new Instances(datalist.get(i),0,datalist.get(i).numInstances());
                 this.clustersSelected.add(Seleted);
             }
        }
        
        return copy;
    }
    
}


