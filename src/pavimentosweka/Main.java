/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pavimentosweka;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;
import weka.core.Instances;
import weka.classifiers.functions.LinearRegression;
import weka.core.EuclideanDistance;
import weka.core.Instance;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Normalize;


/**
 *
 * @author Pacho328
 */

public class Main {
    
   
    
    public static void main(String[] args) throws Exception {
        double bestRES=0.0;
        int bestNC =0;
        double bestCC=0.0;
        for (double icc = 0.88; icc < 0.89; icc=icc+0.01) {  
            System.out.println("CC: "+icc);
            for (int numberOfClusters = 8; numberOfClusters < 9; numberOfClusters++) {             
                Logica logica = new Logica(numberOfClusters,icc);
                logica.agrupacionFase1();
                logica.obtenerGruposLibres();
                Instances Newicfree = logica.gruposHuerfanos();
                logica.agrupacionFase2(Newicfree);
                double ccfinal = logica.calculateCCAverage(logica.ccSelected,logica.clustersSelected);
                System.out.println(numberOfClusters+";"+ccfinal+";"+logica.clustersSelected.size());
                 if(bestRES < ccfinal){
                       bestRES = ccfinal;
                       bestNC = numberOfClusters;
                       bestCC = icc;
                 }
                 logica.evaluarModelos();
            }
        }
           

    }
    
}
