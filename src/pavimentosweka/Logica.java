/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pavimentosweka;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import weka.classifiers.functions.LinearRegression;
import weka.core.EuclideanDistance;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVSaver;

/**
 *
 * @author Pacho328
 */
public class Logica {
    
    public ArrayList<Instances> clustersSelected = new ArrayList<Instances>();
    public ArrayList<Double> ccSelected = new ArrayList<Double>();
    public ArrayList<Instances> clustersFree = new ArrayList<Instances>();
    public ArrayList<Double> ccFree = new ArrayList<Double>();
    public WekaPaviments Paviments ;
    public Instances  data;
    public double initialCC =1;
    public double  newCC=0,ccMin=10;
    public int cont=0;
    public int numberOfClustersT;
    public double icc ;
    public double[] copyGH;

    public Logica(int numberOfClusters, double icc) throws Exception {
        this.Paviments = new WekaPaviments(numberOfClusters, 14638, icc);
        this.data = Paviments.initializeDataSet();
        this.data = Paviments.createIndex(data);
        this.numberOfClustersT = numberOfClusters;
        this.icc = icc;
    }
    
    public void agrupacionFase1() throws Exception{
    
        while(round(initialCC,9)!=round(newCC,9)){
                        if(data.numAttributes()>16){
                                data.deleteAttributeAt(16);
                                data.deleteAttributeAt(16);
                        }
                        for (int i = 0; i < numberOfClustersT; i++) {

                            LinearRegression rlm =  Paviments.calculateRML(data);
                            data = Paviments.predicting(data,rlm,i);
                            data = Paviments.sortByError(data);
                            data = Paviments.saveRemoveCluster(data,rlm,i);    
                        }

                        double fit = Paviments.getFitness(null);
                        Instances NI = Paviments.updateClusters(Paviments.clusterslist);

                        for (int i = 0; i < Paviments.ccSeleted.size(); i++) {
                              ccSelected.add(Paviments.ccSeleted.get(i));
                        }
                        ccFree = Paviments.ccFree;
                        if(cont==0){
                            initialCC =Paviments.correlation;
                            ccMin = initialCC;
                        }else{
                            initialCC=newCC;
                            newCC=Paviments.correlation;
                            if(newCC<ccMin){
                                ccMin=newCC;
                            }else{
                                if(newCC==ccMin){
                                    break;
                                }
                            }
                        }    
                        Instances NewInstances = new Instances(NI,0,NI.numInstances());
                        numberOfClustersT = numberClustersOptimal(NI.numInstances());  
                        for (int i = 0; i < Paviments.clustersSelected.size(); i++) {
                              clustersSelected.add(Paviments.clustersSelected.get(i));
                        }
                        clustersFree = Paviments.clustersFree;
                        Paviments = new WekaPaviments(numberOfClustersT, NewInstances.numInstances(), icc);
                        data= NewInstances;
                        cont++;
                }
        
    }
    
    public void obtenerGruposLibres(){
         for (int i = 0; i < clustersFree.size(); i++) {
                    if(ccFree.get(i)>Paviments.indice2CC){
                        clustersSelected.add(clustersFree.get(i));
                        ccSelected.add(ccFree.get(i));   
                        clustersFree.remove(i);
                        ccFree.remove(i);
                        i--;
                    }
                }   
    }
    public Instances gruposHuerfanos(){
       Instances icfree = joinClustersFree(clustersFree);
       Instances Newicfree = new Instances(icfree,0,icfree.numInstances());
       this.copyGH = new double[ccSelected.size()];
       for (int i = 0; i < ccSelected.size(); i++) {
           this.copyGH[i]= ccSelected.get(i);
       }
       return Newicfree;
    }
    
    public void agrupacionFase2(Instances Newicfree) throws Exception{
             int grupo=0;
                    double[] newCCa;
                    double[] diff = new double[ccSelected.size()]; 
                    for (int i = 0; i < Newicfree.numInstances(); i++) {
                          Instance insTem = Newicfree.get(i);
                          ArrayList<Instances> CopyTem= new  ArrayList<Instances>(clustersSelected);
          //                System.out.println("Selected");
                           for (int j = 0; j < clustersSelected.size(); j++) {
                              CopyTem.get(j).add(insTem);
                          }
                          newCCa = calculateFitness2(CopyTem,icc);
                          for (int k = 0; k < newCCa.length; k++) {
                              diff[k]= newCCa[k] - this.copyGH[k];
                          }
                          double mayor = diff[0];

                          for (int ii = 0; ii < diff.length; ii++) {
                              if(diff [ii] > mayor) {
                                  mayor = diff[ii];
                                  grupo = ii;
                              }
                          }
                          ccSelected.set(grupo, newCCa[grupo]);
                          for (int j = 0; j < clustersSelected.size(); j++) {
                              if(j!=grupo){
                                  CopyTem.get(j).remove(CopyTem.get(j).numInstances()-1);
                              }                
                          }
                    }       
    }
     
    public int numberClustersOptimal(double nInstances){
        int nClus = (int)nInstances/800 ;        
        return nClus; 
    }
    
    public void evaluarModelos() throws IOException, Exception{
        ArrayList<LinearRegression> models = calculateFitness3(clustersSelected,icc);
        Locale spanish2 = new Locale("es", "ES");
        System.out.println("Iniciando Testing...");
        WekaPaviments PavimentsTesting = new WekaPaviments(numberOfClustersT, 14638, icc);
        Instances dataTesting = PavimentsTesting.initializeDataTest();
        System.out.println("Predicciones: "); 
        System.out.println("Pre: "+dataTesting.numInstances());
        dataTesting.deleteAttributeAt(0);
        String []resultadosp = new String[dataTesting.numInstances()];
        for (int i = 0; i < dataTesting.numInstances(); i++) {
          int clust = lowerDistance(clustersSelected,dataTesting.get(i));
          double pred = models.get(clust).classifyInstance(dataTesting.get(i));
          NumberFormat nf = NumberFormat.getInstance(spanish2);
          nf.setMaximumFractionDigits(7);
          nf.setRoundingMode(RoundingMode.UP);
          resultadosp[i] =((String)NumberFormat.getInstance(spanish2).format(dataTesting.get(i).value(0))+";"+ (String)nf.format(pred));
        }
        saveVectorCsv(resultadosp);
        System.out.println("Guardado");
    }
    
    
    public double calculateCCAverage(ArrayList<Double> ccSelected,ArrayList<Instances> clustersSelected ){
        double sum=0,cant,cc,total=0,res ;
        for (int i = 0; i < ccSelected.size(); i++) {
            cc = ccSelected.get(i);
            cant = clustersSelected.get(i).numInstances();
            total = total+cant;
            sum= sum + (cant* cc);     
        }
       res= sum/total;
       return res;
    }
    
    public Instances joinClustersFree(ArrayList<Instances> clustersFree){
        Instances copy = clustersFree.get(0).stringFreeStructure();  
        for (int i = 0; i < clustersFree.size(); i++) {
            for (int j = 0; j < clustersFree.get(i).numInstances(); j++) {
                copy.add(clustersFree.get(i).get(j));
            }
            
        }
        return copy;
    }
    
    public static double[] calculateFitness2(ArrayList<Instances> clist, double icc) throws Exception{
        double[] res;
        WekaPaviments pavementTemp = new WekaPaviments(clist.size(),14638,icc);
        res=pavementTemp.getFitness3(clist);
      
        return res;
    }
      public static ArrayList<LinearRegression> calculateFitness3(ArrayList<Instances> clist, double icc) throws Exception{
        ArrayList<LinearRegression> res;
        WekaPaviments pavementTemp = new WekaPaviments(clist.size(),14638,icc);
        res=pavementTemp.getFitnesSummary(clist);
      
        return res;
    }
      
    public int lowerDistance(ArrayList<Instances> clustersSelected, Instance row){
        int group   =   0;
        double disTem =99999;
        double distance;
        for (int i = 0; i < clustersSelected.size(); i++) {
            if(clustersSelected.get(i).numAttributes()>15){
                clustersSelected.get(i).deleteAttributeAt(0);
            }
            EuclideanDistance Dist = new EuclideanDistance(clustersSelected.get(i));
            for (int j = 0; j < clustersSelected.get(i).numInstances(); j++) {
               Instance instt = clustersSelected.get(i).get(j);
                distance = Dist.distance(row,instt );
              if(distance<disTem){
                    disTem = distance;
                    group=i;
                } 
            }
           
        }
        return group;
    }
     public int lowerDistanceCentroid(Instances clustersSelected, Instance row){
        int group   =   0;
        double disTem =99999;
        double distance;
            EuclideanDistance Dist = new EuclideanDistance(clustersSelected);
            for (int j = 0; j < clustersSelected.numInstances(); j++) {
               Instance instt = clustersSelected.get(j);
                distance = Dist.distance(row,instt );
                if(distance<disTem){
                    disTem = distance;
                    group=j;
                } 
            }
           
        return group;
    }
    
//    
    public void saveVectorCsv(String[] vect) throws IOException{
         
     BufferedWriter br = new BufferedWriter(new FileWriter("data/predicciones.csv"));
        StringBuilder sb = new StringBuilder();

        // Append strings from array
        for (String element : vect) {
         sb.append(element+"\n");
        }

        br.write(sb.toString());
        br.close();
    
    }
    public void saveDataToCsvFile(String path, Instances data) throws IOException{
	    System.out.println("\nSaving to file " + path + "...");
	    CSVSaver saver = new CSVSaver();
	    saver.setInstances(data);
	    saver.setFile(new File(path));
	    saver.writeBatch();
    }
       public static void saveDataToArffFile(String path, Instances data) throws IOException{
	    System.out.println("\nSaving to file " + path + "...");
	    ArffSaver saver = new ArffSaver();
	    saver.setInstances(data);
	    saver.setFile(new File(path));
	    saver.writeBatch();
    }
    public double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }
    
}
